# Some (_quick_) analysis notes about Ehteraz and Hayya to Qatar 2022
**_Last Updated :_**
- **_November 17th, 2022_**    - _added online notes_

\
Workers, players and fans attending the FIFA football World Cup 2022 in Qatar will be asked to install two mobile apps - **Ehteraz** and **Hayya to Qatar 2022** - on their smartphones.

Over the past weeks I have read some press articles where their authors expressed high concern that these two applications put users' privacy & security at serious risk.

Some authors have even labeled such 2 mobile applications as state-sponsored spyware, because - _according to their sources_ - these apps can _uninstall existing applications, make calls, delete photos that the authorities do not want shared..._

\
However, there aren't available any public-domain technical reports, that confirm such claims.

Even the German Federal Commissioner for Data Protection and Freedom of Information - **BfDi** - has carried out only a preliminary analysis for its [recent press short-message about Ehteraz and Hayya](https://www.bfdi.bund.de/SharedDocs/Kurzmeldungen/DE/2022/22_Etheraz-Hayya.html). 


\
Such situation reminded me of what happened some months ago with the Beijing Winter 2022 Olympics MY2022 mobile app.

When some authors even wrote that MY2022 was some kind of disguised spyware, that secretly would even intercept and record the users' conversations... only because they relied on the flawed conclusions of a superficial analysis, that even included rookie mistakes. 

[An in-depth static + runtime analysis was indeed enough to show how most expressed security/privacy concerns about MY2022 app were anyway unsubstantiated](https://gitlab.com/rawmain/my_2022).

\
So I've checked the current & former Android versions of Ehteraz and Hayya.

Just to understand if we're dealing with a MY2022 deja-vu... or if there are for real some critical security/privacy issues while using such 2 mobile apps NOW - during the World Cup 2022 events.

----------------

### Index

- 1. [Let's download the latest Android release(s) of Ehteraz and Hayya](#1-lets-download-the-latest-android-releases-of-ehteraz-and-hayya)

- - 1.1 [Which release should I download?](#11-which-release-should-i-download)

- - 1.2 [Online analysis references](#12-online-analysis-references)

- - 1.3 [Some analysis tips](#13-some-analysis-tips)

----------------

- 2. [Ehteraz](#2-ehteraz)

- - 2.1 [Infos](#21-infos)

- - 2.2 [Permissions](#22-permissions)

- - 2.3 [Start me up!](#23-start-me-up)

----------------

- 3. [Hayya](#3-hayya)

- - 3.1 [Infos](#31-infos)

- - 3.2 [Permissions](#32-permissions)

- - 3.3 [Start me up!](#33-start-me-up)

-----------------

-----------------
-----------------
-----------------

## 1. Let's download the latest Android release(s) of MY Ehteraz and Hayya

### 1.1 Which release should I download?

\
While I'm writing these notes there are officially available for download :

- **Ehteraz for Android** | 2 official builds - 1 packaging format (_Universal APK_)

| Ehteraz - Build#  | Release | Package Deployment                                             | Release Date      | 
|-------------------|---------|----------------------------------------------------------------|-------------------|
| #3032120          | 12.4.1  | Universal APK for armeabi-v7a / arm64-v8a / x86 / x86_64 arch  | May 16, 2022      |
| #3032126          | 12.4.7  | Universal APK for armeabi-v7a / arm64-v8a / x86 / x86_64 arch  | Sep  5, 2022      |

> _Same basic/main code. Release 12.4.7 build #3032126 adds few bugfixes & doesn't include activities/services for Huawei Mobile Services / AppGallery full integration._

-----------------

- **Hayya to Qatar 2022 for Android** | 1 official build - 2 packaging formats (_Universal APK & Bundle AAB_)

| Hayya - Build#    | Release    | Package Deployment                                             | Release Date      |
|-------------------|------------|----------------------------------------------------------------|-------------------|
| #3755             | 8.3.0.3755 | Universal APK for armeabi-v7a / arm64-v8a / x86 / x86_64 arch  | Nov 17, 2022      |
| #3755             | 8.3.0.3755 | Bundle AAB for armeabi-v7a / arm64-v8a / x86 / x86_64 arch     | Nov 16, 2022      |

> _Same full code. Just different app-packaging formats._

-----------------

\
These are the main official download sources for **Ehteraz** - together with the references of their latest available release builds.

| Source for Ehteraz  |Release| Build  | Link                                                          |
|---------------------|-------|--------|---------------------------------------------------------------|
| Huawei AppGallery   |12.4.1 |#3032120| https://appgallery.huawei.com/app/C102110393                  |
|                     |       |        |                                                               |
| Google PlayStore    |12.4.7 |#3032126| https://play.google.com/store/apps/details?id=com.moi.covid19 |

-----------------

\
These are the main official download sources for **Hayya to Qatar 2022** - together with the references of their latest available release builds.

| Source for Hayya    | Release     | Build       | Link                                                          |
|---------------------|-------------|-------------|---------------------------------------------------------------|
| Huawei AppGallery   | 8.3.0.3755  | #3755       | https://appgallery.huawei.com/app/C106576811                  |
|                     |             |             |                                                               |
| Google PlayStore    | 8.3.0.3755  | #3755       | https://play.google.com/store/apps/details?id=com.pl.qatar    |

-----------------
-----------------

### 1.2 Online analysis references

\
Since I've also uploaded the [samples of current & former releases](samples) for both apps to online scanner engines, these are some online-analysis references for release-builds of Ehteraz and Hayya to Qatar 2022.

- **Ehteraz 12.4.7 - Universal APK - Google Play Store**

| Analysis   | Link                                                                                                 |
|------------|------------------------------------------------------------------------------------------------------|
| VirusTotal | https://www.virustotal.com/gui/file/65c28ba6136a95c5734df9fef16b2484144e4da2c77eb89a4c8dcea7fe014b17 |
| ApkLab     | https://apklab.io/apk.html?hash=65c28ba6136a95c5734df9fef16b2484144e4da2c77eb89a4c8dcea7fe014b17     |
| Pithus     | https://beta.pithus.org/report/65c28ba6136a95c5734df9fef16b2484144e4da2c77eb89a4c8dcea7fe014b17      |
| JoeSandbox | https://www.joesandbox.com/analysis/747942/0/html                                                    |
| TriAge     | https://tria.ge/221116-yx1mragg2v                                                                    |

-----------------

- **Ehteraz 12.4.1 - Universal APK - Huawei AppGallery**

| Analysis   | Link                                                                                                 |
|------------|------------------------------------------------------------------------------------------------------|
| VirusTotal | https://www.virustotal.com/gui/file/1d6c4a1ff5b8333ce47e9a5c8e57189add83de519b59af1fd9a78c795a45ca64 |
| ApkLab     | https://apklab.io/apk.html?hash=1d6c4a1ff5b8333ce47e9a5c8e57189add83de519b59af1fd9a78c795a45ca64     |
| Pithus     | https://beta.pithus.org/report/1d6c4a1ff5b8333ce47e9a5c8e57189add83de519b59af1fd9a78c795a45ca64      |
| JoeSandbox | https://www.joesandbox.com/analysis/747938/0/html                                                    |
| TriAge     | https://tria.ge/221116-z3p9msgh8z                                                                    |

-----------------

\
Since there have been anyway some changes of Ehteraz's main code/functions during 2020 and 2021, I'm providing the references of all the release-builds, that have been published.

- **ALL release-builds of Ehteraz - since May 2020**

|Release|Build	  |Release Date |ApkLab & VirusTotal Report                                                                       |
|-------|---------|-------------|-------------------------------------------------------------------------------------------------|
|12.4.7 |#3032126 |Sep  9, 2022 |https://apklab.io/apk.html?hash=65c28ba6136a95c5734df9fef16b2484144e4da2c77eb89a4c8dcea7fe014b17 |
|12.4.4 |#3032123 |Jul 20, 2022 |https://apklab.io/apk.html?hash=6b58bd0afa33bfd9e68bf3ce15f7b1e4a0804716bc7a98f78b4a758fa2c1b8e7 |
|12.4.1 |#3032120 |May 15, 2022 |https://apklab.io/apk.html?hash=dba3547dde9c5d3f7ccc54c1462fb3a53743edb334f2a41924fab97a1aad9cdd |
|12.2.5 |#3032105 |Apr 19, 2022 |https://apklab.io/apk.html?hash=c808ea0676ab10a6a118444bfa1fae79d101dfec3daabe62580ff592691940f1 |
|12.2.1 |#3032101 |Mar  1, 2022 |https://apklab.io/apk.html?hash=71145b41407ab2450958317501a402e0a28e21cfb89e6e0ed08f687501c65bb9 |
|12.0.0 |#3032095 |Feb 15, 2022 |https://apklab.io/apk.html?hash=14b816e460bba033fb69e1628e7f92c619745046e0a67153dc7bacf4d011d592 |
| | | | |
|11.4.0 |#3032091 |Dec  6, 2021 |https://apklab.io/apk.html?hash=0159ffea828bad69bc0cbf27952bc655e7d9f41cc3d0c890c006f59f7309a5dd |
|11.3.0 |#3032090 |Dec  2, 2021 |https://apklab.io/apk.html?hash=3ea5dbfdcda987623fd7fbbba69c8f609ed066fc371b30e44b3b9835c632bb10 |
|11.2.0 |#3032088 |Nov 24, 2021 |https://apklab.io/apk.html?hash=fcab708e53d7ba661301115cda9fee8c3f8fff5f2925392b8c442b9a3e273233 |
|11.1.0 |#3032085 |Sep 23, 2021 |https://apklab.io/apk.html?hash=a1fd001f4ffd1f3d8cc093537fef06888722f8810f1537d073edc264e262c0e6 |
|11.0.6 |#3032079 |Sep 14, 2021 |https://apklab.io/apk.html?hash=05ed7966215cd74c1649b4ad20e725749eaeb340045014b5f88e7f9f5b91316d |
|11.0.5 |#3032075 |Jun 28, 2021 |https://apklab.io/apk.html?hash=f5c8a3bd24bf42f8b87cc35e61d5ecb5b64463c04f34954114da090a13f102bd |
|11.0.2 |#3032072 |Jun 20, 2021 |https://apklab.io/apk.html?hash=0df554ffd7ed564c1194dc85d76231306e397e6262c2de4986a7539007dbb938 |
|11.0.0 |#3032070 |Jun  2, 2021 |https://apklab.io/apk.html?hash=a6695fc29972a01cb312d990a8c048eacc863fd072d3607c6e6f30a70af3f806 |
|10.0.5 |#3032068 |May  7, 2021 |https://apklab.io/apk.html?hash=1e9e9c51e204c188a0b949c2fcc3aeef4c9c09616dd925f3a8e12acb97b6b6b7 |
|10.0.2 |#3032062 |Feb 12, 2021 |https://apklab.io/apk.html?hash=b7bd1527ea368feaab2e0991aad8b97e1c0757b6e66c56d37708879867859381 |
| | | | |
| 9.0.2 |#3032040 |Aug 10, 2020 |https://apklab.io/apk.html?hash=d326adbcee040a2114efa309934c404896112d2ad9d0dbb25a6e0e1e6cf19d23 |
| 8.0.4 |#3032036 |Jul  4, 2020 |https://apklab.io/apk.html?hash=ad4134219af4ddf68fabc79b68ff7496d141bb8fa932e664d431606306c56c65 |
| 8.0.3 |#3032035 |Jun 28, 2020 |https://apklab.io/apk.html?hash=e81f3c97afb8dbed767080f36b47f70d9a70c8fd65e43ab580fc0e8cf859a44c |
| 8.0.2 |#3032034 |Jun 24, 2020 |https://apklab.io/apk.html?hash=7730d3d3e276565b58c0ad29987b848bcdba6ad49a12f327241577e3a23b4ea3 |
| 8.0.1 |#3032033 |Jun  9, 2020 |https://apklab.io/apk.html?hash=57f3b4eb7b3d946461eae5972e149d7c8bbb23b4aa4f1a03501a663073d1a0e3 |
| 8.0.0 |#3032032 |Jun  7, 2020 |https://apklab.io/apk.html?hash=c367e4f765eaaee7181d0428addceb23198bf62b140eabac006413a407dea1be |
| 7.0.5 |#3032031 |May 25, 2020 |https://apklab.io/apk.html?hash=5611b46c882c475af5e592f6971f1c99d194b11a3bff18b56861324f71a8e907 |
| 7.0.4 |#3032030 |May 24, 2020 |https://apklab.io/apk.html?hash=89245c06f78fb127a274d9d17f90f7230d2f61ae9803e0b796e46ed1145dbeea |
| 7.0.3 |#3032029 |May 15, 2020 |https://apklab.io/apk.html?hash=092bb0f473cb30e5fcd1f8a4e60d484c4d0f123462d012fad3af9cd5944fedf8 |
| 7.0.2 |#3032028 |May 13, 2020 |https://apklab.io/apk.html?hash=0cddcfe7296ab87ebef99d7122c8e8f5490e579fe40182dfa68cca20d4c8d1bf |
| 7.0.1 |#3032027 |May  5, 2020 |https://apklab.io/apk.html?hash=338a20256b0c04394f71059d521397f259312cbabe1fc852e1a0b830b2c60130 |
| 7.0.0 |#3032026 |May  1, 2020 |https://apklab.io/apk.html?hash=95e9c19cef383d060be98bd232e70ac23ecdb8f648b08a20cdca6e2aa986eecd |


-----------------

\
Hayya releases have been published since January 2022, but there have been significant changes of its main code/functions only since around 2-3 months ago.

So I've only uploaded the release-builds, that have been published since September.

Anyway enough to check Hayya's details/behavior during time - _and before the beginning of the current 'drama' about these 2 apps_.

- **Latest release-builds of Hayya - since September 2022**

| Release    | Build | Release Date      | ApkLab & VirusTotal Report     |
|------------|-------|-------------------|--------------------------------|
| 8.3.0.3755 | #3755 | Nov 17, 2022 | https://apklab.io/apk.html?hash=a8f24122d62138c97c1f5a9619801c9e33b9612575ee3cc067aee7f65af40d08 |
| 8.2.2.3737 | #3737 | Nov 15, 2022 | https://apklab.io/apk.html?hash=d675fcaaa046d5db295246c1e8b21963b05efb417c5d684d866be23b45d62cdb |
| 8.2.1.3703 | #3703 | Nov  8, 2022 | https://apklab.io/apk.html?hash=52534b784df6bf1b28a2c6819cf09462544aa736e44ce740c4200a205193aec0 |
| 8.2.0.3680 | #3680 | Nov  4, 2022 | https://apklab.io/apk.html?hash=36a376e94a1fbd9b8479fec32698716036075e1bb5ebac1815bcfe2157bb2b8a |
| 8.1.1.3628 | #3628 | Oct  26, 2022 | https://apklab.io/apk.html?hash=7a53861540e8e171429f827eb2524868ce6fec1d3e5bc26692b7807a64d460e9 |
| 8.1.0.3620 | #3620 | Oct  25, 2022 | https://apklab.io/apk.html?hash=4553db9c26ff2d49d75eb61ed6528af24242e2a920b7fdf7c3b0017be3e11974 |
| 8.0.0.3482 | #3482 | Oct   7, 2022 | https://apklab.io/apk.html?hash=9ef618cbcb8e210c046fd349cba6b6ca1bea23d12bb7d40a988f8a1846740a49 |
| 7.1.0.3281 | #3281 | Sep 19, 2022| https://apklab.io/apk.html?hash=abb3aae8e8e31dfea5bf16e1d5ae37ebaceb543e6c7619ee4ce5c34ba94ba8c2 |
| 7.0.0.3105 | #3105 | Sep 7, 2022| https://apklab.io/apk.html?hash=6da2180819b4209f8dd928d7b63128b52c8d100151bd2202fb9feaeb5bf2fd83 |


-----------------
-----------------

### 1.3 Some analysis tips

\
Both Ehteraz and Hayya use simple code reflection/obfuscation and don't feature special/extreme anti-debug protection.

Ehteraz can be used indeed on emulators and rooted devices/environments without any issue.

Hayya performs root-detection instead, but it's quite basic. Such root-check is performed indeed only on app-launch stage & it doesn't detect most root-hiding configurations.

So, just in case your root solution/settings couldn't pass Hayya checks, you should just disable root before launching the app, then re-enable it while keeping the app running.

\
This means that there are neither restrictions nor constraints to perform analysis through known methods/approaches for Android apps, such as :

- Verbose logcat
- MITM
- Trace/Debug **(!)**
- Database/File inspection in `/data/user/0/`

> _**(!)** Ehteraz developers have even forgotten to fully disable the `DevSupportManager` React JS runtime debug module inside the published release-packages. They have just set the `remote_js_debug` value as false inside the `com.moi.covid19_preferences.xml` file of the app._
>
> _So Ehteraz tries anyway to connect to a local development server too (listening on TCP port 8081 of IP 127.0.0.1 or 10.0.2.2 or 10.0.3.2 - according to device/VM runtime environment) in order to submit debug logs to its status/message/inspector endpoints._
>
> _Such debug connections obviously fail if/when there is no active local development server listening on the right IP:port._


\
Keep in mind that Hayya can be used worldwide, but **Ehteraz uses IP geofencing to restrict API access** - by allowing it only to incoming connections from Qatar networks. 

Therefore users' connections from other countries' networks to Ehteraz's API host are indeed forbidden (_403 status_).

Anyway, such protection measure can be easily bypassed through VPN/Proxy services (_in order to get a Qatari IP Address_) or API host/endpoint override (_further details in [chapter 2.3](#23-start-me-up)_)

-----------------
-----------------
-----------------

## 2. Ehteraz

### 2.1 Infos

\
Ehteraz was officially released in May 2020 as Qatar COVID-19 contact-tracing app. Subsequently, further functions have been added during time - _according to public health measures/guidelines to fight the COVID-19 pandemic_ - such as the wallet for PCR/RAT test results and vaccination certificate status.

It uses a centralized BT proximity tracing + location tracking solution - **BioTrace** - developed by Orbis (_a Middle-East based Security System Integrator with branches located in Qatar, UAE and Lebanon_).

\
BioTrace relies indeed on the remote server-side processing in a government central management system (CMS) of the following collected data/logs by the users' devices :

1. Bluetooth ([Altbeacon BLE advertising/broadcast](https://altbeacon.org/)) proximity detection/tracing

2. Network/GPS Location tracking

So Ehteraz performs a continuous on-device collection of such data in foreground/background & periodically sends it to the CMS API endpoint.



| QR Code                                                           | Status               | Description |
|-------------------------------------------------------------------|----------------------|-------------|
|![alt text](doc_img/21_ehteraz_green.png "Negative")               |**Negative**              |NO COVID-19 infection/suspect |
|![alt text](doc_img/21_ehteraz_gray.png "Suspected")               |**Suspected**             |User previously had a Green status and is now awaiting the results of a PCR test |
|![alt text](doc_img/21_ehteraz_yellow.png "Quarantine or Reactive")|**Quarantine or Reactive**|User is in quarantine, or has tested as reactive for COVID-19 after having a PCR test |
|![alt text](doc_img/21_ehteraz_red.png "Infected")                 |**Infected**              |User has tested positive for COVID-19 after having a PCR or Rapid Antigen Test |

\
Collected data stay stored on users' devices in a local database inside Ehteraz's app-data directory (_forbidden access to other apps without root or system privileges_) until the remote server returns OK for data exchange/sync.

Once Ehteraz acknowledges the OK for tracing data synchronization with CMS, it resets the related tables/records in the local database.



-----------------
-----------------

### 2.2 Permissions

\
As written before in [chapter 1.1](#11-which-release-should-i-download), Ehteraz release 12.4.7 adds few bugfixes & doesn't include activities/services for Huawei Mobile Services / AppGallery full integration.

This is the resulting general picture of permissions / activities / services for Ehteraz releases 12.4.1 and 12.4.7.

| Release 12.4.7 Build #3032126 = Universal APK - Available from Google Play Store            |
|---------------------------------------------------------------------------------------------|
| ![alt text](doc_img/22_ehteraz_1247_ps_perm.png "EHTERAZ 12.4.7 - Google Play Store")       |

| Release 12.4.1 Build #3032120 = Universal APK - Available from Huawei AppGallery            |
|---------------------------------------------------------------------------------------------|
| ![alt text](doc_img/22_ehteraz_1241_ag_perm.png "EHTERAZ 12.4.1 - Huawei AppGallery")       |

Some permissions can be excluded from the runtime watchlist.

-----------------

\
First, both releases include the following permissions - required in order to use specific APIs of Huawei Mobile Services / AppGallery and Google Play Services / Store.

| Permission                                                               | Description                                       |
|--------------------------------------------------------------------------|---------------------------------------------------|
| `com.huawei.appmarket.service.commondata.permission.GET_COMMON_DATA`     | query the channel ID of HUAWEI AppGallery         |
| `com.google.android.finsky.permission.BIND_GET_INSTALL_REFERRER_SERVICE` | reporting of install referrer info to Google Play |

\
Since such perms are required by the 2 app stores, they are automatically granted. Yet, they aren't strictly related to Ehteraz activities/services.

Users can indeed sideload/install the APK packages & run Ehteraz on any ARM/x86 device running Android Marshmallow 6.0 or higher - even those without HMS (_Huawei Mobile Services_) and GMS (_Google Play Services_) onboard. 

-----------------

\
Second, owing to app-dependencies there is also the manifest-declaration of some deprecated permissions.

According to minSdk (API 23 Marshmallow 6.0) and targetSdk (API 30 11/R) references & requirements, these deprecated perms can't be granted to third-party apps without further special privileges = only if such third-party apps have been installed/set as system-apps or manually authorized by the user.

| Permission                                    |12.4.7|12.4.1| Description                         |
|-----------------------------------------------|------|------|-------------------------------------|
| `android.permission.SYSTEM_ALERT_WINDOW`      | ✅   | ✅  | show system-alert windows           |
| `android.permission.REQUEST_INSTALL_PACKAGES` | ❌   | ✅  | request installing packages         |
| `android.permission.QUICKBOOT_POWERON`        | ✅   | ✅  | resume downloads after a quick boot |
| `android.permission.reboot`                   | ✅   | ✅  | reboot the device                   |

> _✅ = perm-declaration_
> _❌ = NO perm-declaration_

\
Users should indeed manually enable/grant system-special settings (_special permissions for apps_) to Ehteraz and/or set it even as a device-administration app in order to allow such permissions too.

Ehteraz app doesn't require such extra special permissions at all for its operations. Besides, there are no in-app alerts/messages either, requesting the users to allow/enable such perms.

-----------------

\
Therefore, the runtime-watchlist can focus only on the remaining (14) perms. Since such perms have different risk-levels, let's group them according to HIGH/LOW perm level & to their runtime-usage by Ehteraz.

-----------------

\
**A.** High Level perms

| **PERM Control Group A**                            | Level  | Description                                               |
|-----------------------------------------------------|--------|-----------------------------------------------------------|
| `android.permission.ACCESS_BACKGROUND_LOCATION`     | HIGH   | location in background (**requires granted coarse/fine**) |
| `android.permission.ACCESS_COARSE_LOCATION`         | HIGH   | coarse (network-based) location                           |
| `android.permission.ACCESS_FINE_LOCATION`           | HIGH   | fine (GPS) location                                       |
| `android.permission.CALL_PHONE`                     | HIGH   | directly call phone numbers                               |

These perms require user's intervention in order to be granted & are NOT compulsory - _unless the user must undergo a period of isolation because of a positive PCR/RAT test-result at an approved medical centre_.

As explained in the next chapter, **users who haven't tested positive for COVID-19 can indeed deny/reject all such 4 perms - without affecting Ehteraz's basic/main functionality at all**.

-----------------

\
**B.** Low level perms

| **PERM Control Group B**                            | Level  | Description                                               |
|-----------------------------------------------------|--------|-----------------------------------------------------------|
| `android.permission.DISABLE_KEYGUARD`               | LOW    | disable the keyguard if it is not secure                  |
| `android.permission.ACCESS_NETWORK_STATE`           | LOW    | view network connectivity status                          |
| `android.permission.ACCESS_WIFI_STATE`              | LOW    | view Wi-Fi status                                         |
| `android.permission.BLUETOOTH`                      | LOW    | create Bluetooth connections                              |
| `android.permission.BLUETOOTH_ADMIN`                | LOW    | bluetooth administration (_discover and pair BT devices_) |
| `android.permission.FOREGROUND_SERVICE`             | LOW    | allow non-system app to use Service.startForeground       |
| `android.permission.INTERNET`                       | LOW    | full internet access                                      |
| `android.permission.RECEIVE_BOOT_COMPLETED`         | LOW    | automatically start at boot                               |
| `android.permission.VIBRATE`                        | LOW    | control vibrator                                          |
| `android.permission.WAKE_LOCK`                      | LOW    | prevent phone from sleeping                               |

-----------------



